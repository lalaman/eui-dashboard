import { useEffect, useState, Fragment } from 'react';
import {
  EuiEmptyPrompt,
  EuiButton,
  EuiIcon,
  EuiLoadingLogo,
  useEuiTheme
} from '@elastic/eui';
import ProjectService from '../../services/project.service';
import ProjectModal from '../../components/ProjectModal';
import ProjectsTable from './ProjectsTable';

const ProjectsPage = () => {
  const { euiTheme: theme } = useEuiTheme();
  const [projects, setProjects] = useState([]);
  const [currentProject, setCurrentProject] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);

  // Close all modals
  const closeModal = () => {
    setCurrentProject(null);
    setShowModal(false);
  };

  // Fetch projects
  const fetchProjects = async () => {
    setLoading(true);
    const res = await ProjectService.fetchAll().catch((err) => {
      console.log(err);
    });

    setProjects(res.data);
    setLoading(false);
  };

  useEffect(() => {
    fetchProjects();
  }, []);

  return (
    <Fragment>
      {(() => {
        // Loading
        if (loading) {
          return (
            <div
              style={{ marginTop: theme.size.xxl }}
              className="eui-textCenter"
            >
              <EuiLoadingLogo logo="logoElasticStack" size="l" />
            </div>
          );
        }

        // No projects found
        else if (!projects.length && !loading) {
          return (
            <EuiEmptyPrompt
              icon={<EuiIcon type="logoAppSearch" size="original" />}
              title={
                <div className="eui-textMediumShade">No projects found</div>
              }
              titleSize="xs"
              actions={
                <EuiButton
                  color="primary"
                  fill
                  size="s"
                  onClick={() => setShowModal('CREATE_PROJECT')}
                >
                  Create project
                </EuiButton>
              }
            />
          );
        }

        // Content
        return (
          <ProjectsTable
            projects={projects}
            create={() => setShowModal('CREATE_PROJECT')}
            fetch={fetchProjects}
          />
        );
      })()}

      {(() => {
        // Project create modal
        if (showModal === 'CREATE_PROJECT')
          return (
            <ProjectModal
              project={currentProject}
              onClose={closeModal}
              fetch={fetchProjects}
            />
          );
      })()}
    </Fragment>
  );
};

export default ProjectsPage;
