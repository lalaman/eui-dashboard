import { useMemo } from 'react';
import { NavLink } from 'react-router-dom';
import {
  EuiBasicTable,
  EuiIcon,
  EuiText,
  EuiBetaBadge,
  EuiPanel,
  EuiFlexGroup,
  EuiFlexItem,
  EuiButton,
  EuiSpacer
} from '@elastic/eui';

const ProjectsTable = (props) => {
  const projects = useMemo(() => props.projects, [props.projects]);
  const createProject = props.create;

  const columns = [
    {
      field: 'icon',
      name: '',
      width: '40px',
      render: (_, project) => (
        <EuiBetaBadge
          label={project.name.charAt(0)}
          color="accent"
          style={{ padding: 0 }}
        />
      )
    },
    {
      field: 'name',
      name: 'Project',
      sortable: true,
      render: (name, project) => (
        <div>
          <NavLink to={`/project/${project.id}`}>
            <EuiText
              size="xs"
              color="text"
              className="eui-displayBlock"
              style={{ fontWeight: 700 }}
            >
              {name}
            </EuiText>
          </NavLink>
          <EuiText size="xs" color="light">
            {project.developer.name}
          </EuiText>
        </div>
      )
    },
    {
      field: 'address',
      name: 'Location',
      truncateText: true,
      render: (address, project) => (
        <div>
          {address.street ? (
            <EuiText size="xs" className="eui-displayBlock">
              {address.street_alt ? `${address.street_alt}-` : ''}
              {address.street}
            </EuiText>
          ) : (
            ''
          )}
          <EuiText size="xs" className="eui-displayBlock">
            {address.city}
            {address.state ? `, ${address.state}` : ''}
          </EuiText>
        </div>
      )
    },
    {
      field: 'actions',
      name: 'Actions',
      align: 'right',
      render: (_, project) => (
        <div>
          <EuiIcon type="boxesHorizontal"></EuiIcon>
        </div>
      )
    }
  ];

  return (
    <EuiPanel hasBorder>
      <EuiFlexGroup alignItems="flexStart">
        <EuiFlexItem grow={false}>
          <EuiButton onClick={createProject} fill size="s">
            New project
          </EuiButton>
        </EuiFlexItem>
      </EuiFlexGroup>

      <EuiSpacer size="l" />

      <EuiBasicTable
        tableCaption="Projects"
        items={projects}
        columns={columns}
      />
    </EuiPanel>
  );
};

export default ProjectsTable;
