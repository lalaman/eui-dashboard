import { EuiFlexGroup, EuiFlexItem, useEuiTheme } from '@elastic/eui';
import { Fragment } from 'react';
import { Navigate, Routes, Route } from 'react-router-dom';

import Header from '../components/Header';
import SideMenu from '../components/SideMenu';
import DevelopersPage from './DevelopersPage/DevelopersPage';
import ProjectsPage from './ProjectsPage/ProjectsPage';
import ProjectDetailsPage from './ProjectDetailsPage/ProjectDetailsPage';
import SettingsPage from './SettingsPage';

const HomePage = () => {
  const { euiTheme: theme } = useEuiTheme();

  return (
    <Fragment>
      <Header />

      <div
        style={{
          maxWidth: theme.breakpoint.l,
          margin: '0 auto',
          overflow: 'hidden'
        }}
      >
        <EuiFlexGroup style={{ paddingTop: theme.size.xxxxl }}>
          {/** Side menu */}
          <SideMenu />

          {/* Main content */}
          <EuiFlexItem style={{ padding: theme.size.l }}>
            <Routes>
              <Route
                exact
                path="/"
                element={<Navigate to="/projects" replace />}
              />
              <Route  path="developers" element={<DevelopersPage />} />
              <Route path="projects" element={<ProjectsPage />} />
              <Route path="settings" element={<SettingsPage />} />
              <Route path="project/:project_id" element={<ProjectDetailsPage />} />
            </Routes>
          </EuiFlexItem>
        </EuiFlexGroup>
      </div>
    </Fragment>
  );
};

export default HomePage;
