import { Fragment, useMemo, useState } from 'react';
import {
  EuiBasicTable,
  EuiButtonIcon,
  EuiText,
  EuiBetaBadge,
  EuiPanel,
  EuiFlexGroup,
  EuiFlexItem,
  EuiButton,
  EuiSpacer,
  EuiPopover,
  EuiContextMenuPanel,
  EuiContextMenuItem,
  EuiModal,
  EuiModalHeader,
  EuiModalHeaderTitle,
  EuiModalBody,
  EuiModalFooter,
  EuiButtonEmpty,
  useEuiTheme
} from '@elastic/eui';
import DeveloperService from '../../services/developer.service';
import { addToast } from '../../components/ToastController';

const DevelopersTable = (props) => {
  const createDeveloper = props.create;
  const { euiTheme: theme } = useEuiTheme();
  const [sortField, setSortField] = useState('name');
  const [sortDirection, setSortDirection] = useState('asc');
  const [selectedDeveloper, setSelectedDeveloper] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);
  const setCurrentDeveloper = props.set;
  const fetchDevelopers = props.fetch;

  const panelItems = [
    <EuiContextMenuItem
      key="edit"
      onClick={() => {
        setCurrentDeveloper(selectedDeveloper);
        setSelectedDeveloper(null);
      }}
    >
      Edit
    </EuiContextMenuItem>,
    <EuiContextMenuItem
      key="remove"
      onClick={() => {
        setShowModal('REMOVE_DEVELOPER');
      }}
      style={{ color: theme.colors.danger }}
    >
      Remove
    </EuiContextMenuItem>
  ];

  const columns = [
    {
      field: 'icon',
      name: '',
      width: '40px',
      render: (_, developer) => (
        <EuiBetaBadge
          label={developer.name.charAt(0)}
          color="accent"
          style={{ padding: 0 }}
        />
      )
    },
    {
      field: 'name',
      name: 'Developer',
      sortable: true,
      render: (name) => (
        <div>
          <EuiText
            size="xs"
            color="text"
            className="eui-displayBlock"
            style={{ fontWeight: 700 }}
          >
            {name}
          </EuiText>
        </div>
      )
    },
    {
      field: 'address',
      name: 'Location',
      truncateText: true,
      render: (address) => (
        <div>
          <EuiText size="xs" className="eui-displayBlock">
            {address.city || 'TBD'}
          </EuiText>
        </div>
      )
    },
    {
      field: 'projects_count',
      name: 'Projects',
      align: 'right',
      sortable: true,
      render: (projects_count) => <Fragment>{projects_count}</Fragment>
    },
    {
      field: 'actions',
      name: 'Actions',
      align: 'right',
      render: (_, developer) => (
        <EuiPopover
          id={`developer-${developer.id}`}
          button={
            <EuiButtonIcon
              aria-label="Actions"
              iconType="boxesHorizontal"
              onClick={() => {
                setSelectedDeveloper(selectedDeveloper ? null : developer);
              }}
            />
          }
          isOpen={selectedDeveloper?.id === developer.id}
          closePopover={() => setSelectedDeveloper(null)}
          panelPaddingSize="none"
          hasArrow={false}
          anchorPosition="downRight"
        >
          <EuiContextMenuPanel items={panelItems} />
        </EuiPopover>
      )
    }
  ];

  const developers = useMemo(() => {
    if (!props?.developers?.length) return [];
    const devs = [...props.developers];

    // Sort by name
    if (sortField === 'name')
      return sortDirection === 'asc'
        ? devs.sort((a, b) => a.name.localeCompare(b.name))
        : devs.sort((a, b) => b.name.localeCompare(a.name));

    // Sort by projects
    if (sortField === 'projects_count') {
      console.log('sorting by projects count');
      return sortDirection === 'asc'
        ? devs.sort((a, b) => a.projects_count - b.projects_count)
        : devs.sort((a, b) => b.projects_count - a.projects_count);
    }
  }, [props.developers, sortDirection, sortField]);

  const onTableChange = ({ sort = {} }) => {
    const { field: sortField, direction: sortDirection } = sort;
    setSortField(sortField);
    setSortDirection(sortDirection);
  };

  const removeDeveloper = () => {
    setIsDeleting(true);
    DeveloperService.remove(selectedDeveloper.id)
      .then(() => {
        fetchDevelopers();
        addToast({
          title: 'Remove developer',
          color: 'success',
          iconType: 'check',
          text: (
            <p>
              Successfully removed{' '}
              <strong>{selectedDeveloper.name}</strong>
            </p>
          )
        });
      })
      .catch((err) => {
        console.log(err);
        addToast({
          title: 'Failed to remove developer',
          color: 'danger',
          iconType: 'cross',
          text: <p>Unable to remove {selectedDeveloper.name}</p>
        });
      })
      .finally(() => {
        setIsDeleting(false);
        setShowModal(false);
        setSelectedDeveloper(null);
      });
  };

  return (
    <Fragment>
      <EuiPanel hasBorder>
        <EuiFlexGroup alignItems="flexStart">
          <EuiFlexItem grow={false}>
            <EuiButton onClick={createDeveloper} fill size="s">
              New developer
            </EuiButton>
          </EuiFlexItem>
        </EuiFlexGroup>

        <EuiSpacer size="l" />

        <EuiBasicTable
          tableCaption="Developers"
          items={developers}
          columns={columns}
          sorting={{
            sort: {
              field: sortField,
              direction: sortDirection
            }
          }}
          onChange={onTableChange}
        />
      </EuiPanel>

      {/* Remove developer modal  */}
      {(() => {
        if (showModal === 'REMOVE_DEVELOPER') {
          return (
            <EuiModal
              onClose={() => setShowModal(false)}
              initialFocus="[name=popswitch]"
            >
              <EuiModalHeader>
                <EuiModalHeaderTitle
                  style={{
                    fontWeight: 'bold'
                  }}
                >
                  Remove Developer
                </EuiModalHeaderTitle>
              </EuiModalHeader>

              <EuiModalBody>
                Are you sure you want to remove <strong>{selectedDeveloper?.name}</strong>?
              </EuiModalBody>
              <EuiModalFooter>
                <EuiButtonEmpty onClick={() => setShowModal(false)}>
                  Cancel
                </EuiButtonEmpty>

                <EuiButton
                  color="danger"
                  form="project-modal"
                  isLoading={isDeleting}
                  fill
                  onClick={removeDeveloper}
                >
                  Remove
                </EuiButton>
              </EuiModalFooter>
            </EuiModal>
          );
        }
      })()}
    </Fragment>
  );
};

export default DevelopersTable;
