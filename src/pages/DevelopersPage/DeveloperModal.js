import { useEffect, useMemo, useRef, useState } from 'react';
import _ from 'lodash';
import {
  EuiModal,
  EuiModalHeader,
  EuiModalHeaderTitle,
  EuiModalBody,
  EuiForm,
  EuiFormRow,
  EuiFieldText,
  EuiModalFooter,
  EuiButton,
  EuiButtonEmpty,
  useEuiFontSize
} from '@elastic/eui';
import DeveloperService from '../../services/developer.service';
import { addToast } from '../../components/ToastController';

const DeveloperModal = (props) => {
  const nameInput = useRef();
  const form = useRef();
  const [loading, setLoading] = useState(false);
  const [developer, setDeveloper] = useState({
    id: null,
    name: '',
    address: {
      street: '',
      street_alt: '',
      city: '',
      state: '',
      country: '',
      zip: ''
    }
  });
  const closeModal = props.onClose;
  const fetchDevelopers = props.fetch;

  useEffect(() => {
    setDeveloper({
      id: props?.developer?.id || null,
      name: props?.developer?.name || '',
      address: {
        street: props?.developer?.address?.street || '',
        street_alt: props?.developer?.address?.street_alt || '',
        city: props?.developer?.address?.city || '',
        state: props?.developer?.address?.state || '',
        country: props?.developer?.address?.country || '',
        zip: props?.developer?.address?.zip || ''
      }
    });

    setTimeout(() => {
      if (nameInput?.current) nameInput.current.focus();
    });
  }, [props.developer]);

  const validDeveloper = useMemo(() => !!developer.name.trim(), [developer]);

  // Create or update developer
  const handleDeveloperAction = (ev) => {
    ev.preventDefault();
    setLoading(true);

    const createMode = developer.id === null;
    const query = createMode
      ? DeveloperService.create(developer)
      : DeveloperService.update(developer.id, developer);

    query
      .then(() => {
        fetchDevelopers();
        closeModal();
        addToast({
          title: createMode ? 'Added developer' : 'Updated developer',
          color: 'success',
          iconType: 'check',
          text: (
            <p>
              Successfully {createMode ? 'added ' : 'updated '}
              <strong>{developer.name}</strong>
            </p>
          )
        });
      })
      .catch((err) => {
        console.log(err);
        addToast({
          title: 'Failed',
          color: 'danger',
          iconType: 'cross',
          text: <p>Unable to {createMode ? 'create' : 'update'} developer</p>
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <EuiModal onClose={props.onClose} initialFocus="[name=popswitch]">
      <EuiModalHeader>
        <EuiModalHeaderTitle
          style={{
            fontSize: useEuiFontSize('l').fontSize,
            fontWeight: 'bold'
          }}
        >
          {developer?.id ? 'Update' : 'Create'} Developer
        </EuiModalHeaderTitle>
      </EuiModalHeader>

      <EuiModalBody>
        <EuiForm
          id="developer-modal"
          ref={form}
          component="form"
          onSubmit={handleDeveloperAction}
        >
          {/* Name */}
          <EuiFormRow label="Developer name">
            <EuiFieldText
              inputRef={nameInput}
              value={developer.name}
              isInvalid={!developer.name}
              compressed
              onChange={(e) =>
                setDeveloper((p) => ({ ...p, name: e.target.value }))
              }
            />
          </EuiFormRow>
          {
            /** Address fields */
            Object.keys(developer.address)
              .filter((k) => k !== 'id')
              .map((field) => (
                <EuiFormRow
                  key={field}
                  label={(() => {
                    if (field === 'state') return 'State/Province';
                    if (field === 'zip') return 'Zip/Postal code';
                    return _.capitalize(field.replace('_', ' '));
                  })()}
                >
                  <EuiFieldText
                    value={developer.address[field]}
                    compressed
                    onChange={(e) =>
                      setDeveloper((d) => ({
                        ...d,
                        address: { ...d.address, [field]: e.target.value }
                      }))
                    }
                  />
                </EuiFormRow>
              ))
          }
        </EuiForm>
      </EuiModalBody>

      <EuiModalFooter>
        <EuiButtonEmpty onClick={closeModal}>Cancel</EuiButtonEmpty>

        <EuiButton
          type="submit"
          form="developer-modal"
          isLoading={loading}
          isDisabled={!validDeveloper}
          fill
        >
          {(() => {
            if (loading) return '';
            return developer.id ? 'Update' : 'Create';
          })()}
        </EuiButton>
      </EuiModalFooter>
    </EuiModal>
  );
};

export default DeveloperModal;
