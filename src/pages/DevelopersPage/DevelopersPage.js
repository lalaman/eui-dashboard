import {
  useEuiTheme,
  EuiLoadingLogo,
  EuiEmptyPrompt,
  EuiIcon,
  EuiButton
} from '@elastic/eui';
import { useEffect } from 'react';
import { Fragment, useState } from 'react';
import DeveloperService from '../../services/developer.service';
import DeveloperModal from './DeveloperModal';
import DevelopersTable from './DevelopersTable';

const DevelopersPage = () => {
  const { euiTheme: theme } = useEuiTheme();

  const [loading, setLoading] = useState(false);
  const [currentDeveloper, setCurrentDeveloper] = useState(null);
  const [developers, setDevelopers] = useState([]);
  const [showModal, setShowModal] = useState(false);

  // Fetch developers
  const fetchDevelopers = async () => {
    setLoading(true);
    const res = await DeveloperService.fetchAll().catch((err) => {
      console.log(err);
    });

    setDevelopers(res.data);
    setLoading(false);
  };

  // Close all modals
  const closeModal = () => {
    setCurrentDeveloper(null);
    setShowModal(false);
  };

  useEffect(() => {
    fetchDevelopers();
  }, []);

  return (
    <Fragment>
      {(() => {
        // Loading
        if (loading) {
          return (
            <div
              style={{ marginTop: theme.size.xxl }}
              className="eui-textCenter"
            >
              <EuiLoadingLogo logo="logoElasticStack" size="l" />
            </div>
          );
        }

        // No developers
        else if (!loading && !developers.length) {
          return (
            <EuiEmptyPrompt
              icon={<EuiIcon type="logoAppSearch" size="original" />}
              title={
                <div className="eui-textMediumShade">No developers found</div>
              }
              titleSize="xs"
              actions={
                <EuiButton
                  color="primary"
                  fill
                  size="s"
                  onClick={() => setShowModal('CREATE_DEVELOPER')}
                >
                  Add developer
                </EuiButton>
              }
            />
          );
        }

        // Show developer table
        else {
          return (
            <DevelopersTable
              developers={developers}
              create={() => setShowModal('CREATE_DEVELOPER')}
              set={(developer) => {
                setShowModal('CREATE_DEVELOPER');
                setCurrentDeveloper(developer);
              }}
              fetch={fetchDevelopers}
            />
          );
        }
      })()}

      {(() => {
        // Developer create modal
        if (showModal === 'CREATE_DEVELOPER')
          return (
            <DeveloperModal
              developer={currentDeveloper}
              onClose={closeModal}
              fetch={fetchDevelopers}
            />
          );
      })()}
    </Fragment>
  );
};

export default DevelopersPage;
