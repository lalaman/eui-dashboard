import {
  EuiBottomBar,
  EuiButton,
  EuiCheckbox,
  EuiFlexGroup,
  EuiFlexItem,
  EuiSpacer,
  EuiFilePicker,
  useEuiTheme
} from '@elastic/eui';
import { Fragment, useMemo, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import Fancybox from '../../components/Fancybox';
import { addToast } from '../../components/ToastController';
import ProjectService from '../../services/project.service';

const RenderingsTab = (props) => {
  const params = useParams();
  const { euiTheme: theme } = useEuiTheme();
  const heroImageFilePicker = useRef();
  const renderingsFilePicker = useRef();
  const fetchProject = props.fetch;

  const projectId = useMemo(() => params.project_id, [params.project_id]);
  const allRenderings = useMemo(() => props.renderings, [props.renderings]);
  const heroImage = useMemo(() => allRenderings?.hero_image || null);
  const renderings = useMemo(() => allRenderings?.renderings || []);

  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState([]);

  const handleImageUpload = (fileList, type) => {
    if (!fileList?.length) return;
    const payload = new FormData();
    for (const file of fileList) {
      payload.append('renderings', file);
    }
    setLoading(true);

    ProjectService.uploadRenderings(projectId, type, payload)
      .then(fetchProject)
      .finally(() => {
        heroImageFilePicker?.current?.removeFiles();
        renderingsFilePicker?.current?.removeFiles();
        setLoading(false);
      });
  };

  const handleRemoveRenderings = () => {
    if (!!selected.length) {
      const payload = { rendering_ids: selected };
      ProjectService.removeRenderings(projectId, payload)
        .then((res) => {
          if (res) {
            fetchProject();
            setSelected([]);
            addToast({
              title: 'Removed renderings',
              color: 'success',
              iconType: 'check',
              text: <p>Successfully removed {selected.length} renderings</p>
            });
          }
        })
        .finally(() => {
          setLoading(false);
        });
    }
  };

  return (
    <Fragment>
      {/* Hero image */}
      <div style={{ marginTop: theme.size.m }}>
        <div style={{ marginBottom: theme.size.m }}>
          <strong>Hero image</strong>
        </div>

        <div style={{ display: 'none' }}>
          <EuiFilePicker
            ref={heroImageFilePicker}
            accept="image/*"
            id="hero-image-picker"
            display="default"
            onChange={(ev) => handleImageUpload(ev, 'hero')}
          />
        </div>

        {heroImage ? (
          // Display thumbnail
          <div style={{ marginBottom: theme.size.m }}>
            <Fancybox
              options={{ infinite: false }}
              style={{
                opacity: heroImage && loading ? 0.5 : 1,
                pointerEvents: loading ? 'none' : 'all'
              }}
            >
              <div
                src={heroImage.thumbnail}
                alt="Hero"
                data-fancybox="hero-image"
                data-src={heroImage.url}
                style={{
                  cursor: 'pointer',
                  backgroundImage: `url('${heroImage.url}')`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                  width: '150px',
                  height: '150px',
                  borderRadius: '10px'
                }}
                className="hover"
              />
            </Fancybox>
          </div>
        ) : null}

        {/* Upload/Replace button */}
        <EuiButton
          size="s"
          fill={!heroImage}
          isLoading={loading}
          onClick={() => heroImageFilePicker.current.fileInput.click()}
        >
          {heroImage ? 'Replace' : 'Upload'}
        </EuiButton>
      </div>

      {/* Renderings */}
      <div style={{ marginTop: theme.size.xl }}>
        <div style={{ marginBottom: theme.size.m }}>
          <strong>Renderings</strong>
        </div>

        <div style={{ display: 'none' }}>
          <EuiFilePicker
            ref={renderingsFilePicker}
            accept="image/*"
            multiple
            id="renderings-image-picker"
            display="default"
            onChange={(ev) => handleImageUpload(ev, 'rendering')}
          />
        </div>

        {/* Upload/Replace button */}
        <EuiButton
          size="s"
          fill={!renderings.length}
          isLoading={loading}
          onClick={() => renderingsFilePicker.current.fileInput.click()}
        >
          Upload
        </EuiButton>

        <EuiSpacer />

        <EuiFlexGroup
          justifyContent="flexStart"
          alignItems="flexStart"
          wrap
          gutterSize="l"
          direction="row"
        >
          {renderings.map((r) => (
            <EuiFlexItem
              key={r.id}
              grow={false}
              style={{ width: '150px', height: '150px', position: 'relative' }}
            >
              <Fancybox options={{ infinite: false }}>
                {/* Checkbox */}
                <div
                  style={{
                    position: 'absolute',
                    top: theme.size.s,
                    left: theme.size.s,
                    zIndex: 2
                  }}
                >
                  <EuiCheckbox
                    id={r.id}
                    checked={selected.includes(r.id)}
                    onChange={() => {
                      selected.includes(r.id)
                        ? setSelected(selected.filter((id) => id !== r.id))
                        : setSelected([...selected, r.id]);
                    }}
                  />
                </div>

                {/* Rendering overlay */}
                <div
                  style={{
                    background:
                      'linear-gradient(0deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0.2) 100%)',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    height: '50%',
                    width: '100%',
                    zIndex: 1,
                    pointerEvents: 'none'
                  }}
                ></div>

                {/* Rendering image */}
                <div
                  src={r.thumbnail}
                  alt="Rendering"
                  data-fancybox="rendering"
                  data-src={r.url}
                  style={{
                    cursor: 'pointer',
                    backgroundImage: `url('${r.url}')`,
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    display: 'inline-block',
                    borderRadius: '10px',
                    width: '100%',
                    height: '100%'
                  }}
                  className="hover"
                />
              </Fancybox>
            </EuiFlexItem>
          ))}
        </EuiFlexGroup>
      </div>

      {/* Rendering actions */}
      {selected.length > 0 ? (
        <EuiBottomBar
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            background: 'transparent',
            pointerEvents: 'none',
            padding: '0',
            boxShadow: 'none'
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              background: 'rgba(0, 0, 0, 0.95)',
              pointerEvents: 'all',
              height: '100%',
              width: '300px',
              maxWidth: '100%',
              padding: `${theme.size.m} ${theme.size.l}`
            }}
          >
            <div>Selected {selected.length}</div>
            <EuiButton
              fill
              color="accent"
              size="s"
              isLoading={loading}
              onClick={handleRemoveRenderings}
            >
              <span style={{ color: 'white' }}>Remove</span>
            </EuiButton>
          </div>
        </EuiBottomBar>
      ) : null}
    </Fragment>
  );
};

export default RenderingsTab;
