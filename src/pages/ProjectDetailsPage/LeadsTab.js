import { Fragment, useMemo } from 'react';

const LeadsTab = props => {
  const leads = useMemo(() => props.leads, [props.leads]);

  return (
    <Fragment>
      This is the leads page
    </Fragment>
  )
};

export default LeadsTab;
