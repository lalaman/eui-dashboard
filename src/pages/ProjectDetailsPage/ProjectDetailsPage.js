import _ from 'lodash';
import {
  EuiEmptyPrompt,
  EuiButtonEmpty,
  EuiFlexGroup,
  EuiFlexItem,
  EuiText,
  EuiLoadingSpinner,
  EuiTabs,
  EuiTab,
  EuiSpacer,
  useEuiTheme
} from '@elastic/eui';
import { Fragment, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, NavLink } from 'react-router-dom';
import { fetchProject, unsetProject } from '../../store/project';
import InventoryTab from './InventoryTab';
import RenderingsTab from './RenderingsTab';
import LeadsTab from './LeadsTab';
import ProjectModal from '../../components/ProjectModal';

const ProjectDetailsPage = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const { euiTheme: theme } = useEuiTheme();

  const project = useSelector((state) => state.project);
  const projectId = useMemo(() => params.project_id || [], [params.project_id]);

  const tabs = ['inventory', 'renderings', 'leads'];

  const [currentTab, setCurrentTab] = useState(tabs[0]);
  const [status, setStatus] = useState('FETCH_PROJECT');
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    return () => {
      // Unset project before unmount
      unsetProject();
    };
  }, []);

  // Fetch project details
  useEffect(() => {
    _fetchProject();

    // Go to current tab if hash is found
    const hash = window.location.hash.replace('#', '');
    if (hash && tabs.includes(hash)) {
      setCurrentTab(hash);
    }
  }, [projectId]);

  const _fetchProject = () => {
    if (isNaN(projectId)) {
      setStatus(false);
      return;
    }

    setStatus('FETCH_PROJECT');
    dispatch(fetchProject(projectId)).then(() => {
      setStatus(false);
    });
  };

  const currentTabContent = useMemo(() => {
    switch (currentTab) {
      case 'inventory':
        return (
          <InventoryTab inventory={project.inventory} fetch={_fetchProject} />
        );
      case 'renderings':
        return (
          <RenderingsTab
            renderings={project.renderings}
            fetch={_fetchProject}
          />
        );
      case 'leads':
        return <LeadsTab leads={project.leads} fetch={_fetchProject} />;
      default:
        return;
    }
  }, [project, currentTab]);

  return (() => {
    if (!project.id && status === false) {
      // Error
      return (
        <EuiEmptyPrompt
          body={
            <div>
              <div>Project failed to load or does not exist</div>
              <NavLink to="/projects">
                <EuiButtonEmpty color="primary">
                  Back to projects
                </EuiButtonEmpty>
              </NavLink>
            </div>
          }
        />
      );
    } else if (!project.id && status === 'FETCH_PROJECT') {
      // Loading
      return (
        <div style={{ textAlign: 'center' }}>
          <EuiLoadingSpinner size="m" />
        </div>
      );
    }

    // Content
    return (
      <Fragment>
        {/* Header */}
        <EuiFlexGroup alignItems="center" style={{ marginLeft: 0 }}>
          {/* Hero image */}
          <EuiFlexItem grow={false}>
            {project.rendererings?.[0]?.url ? (
              <div src={project.renderings[0].url} alt="Hero" style={{}} />
            ) : (
              <EuiFlexGroup
                alignItems="center"
                justifyContent="center"
                style={{
                  borderRadius: '15px',
                  width: '80px',
                  height: '80px',
                  marginRight: theme.size.xxs,
                  textTransform: 'capitalize',
                  backgroundColor: 'purple',
                  color: 'white'
                }}
              >
                <EuiText size="m">
                  <h2>{project.name[0]}</h2>
                </EuiText>
              </EuiFlexGroup>
            )}
          </EuiFlexItem>
          {/* Project/developer details */}
          <EuiFlexItem>
            <EuiText size="m">
              <h4>{project.name}</h4>
            </EuiText>
            <EuiText size="s" color="gray">
              {project.address.city}
            </EuiText>
            <EuiText size="s">{project.developer.name}</EuiText>
            <div>
              <EuiButtonEmpty
                flush="left"
                color="primary"
                size="xs"
                onClick={() => setShowModal('UPDATE_PROJECT')}
              >
                Edit details
              </EuiButtonEmpty>
            </div>
          </EuiFlexItem>
        </EuiFlexGroup>

        <EuiSpacer></EuiSpacer>

        {/* Tabs */}
        <EuiTabs>
          {tabs.map((tab) => (
            <EuiTab
              key={tab}
              href={`#${tab}`}
              onClick={() => setCurrentTab(tab)}
              isSelected={currentTab === tab}
            >
              <div
                style={{ fontWeight: currentTab === tab ? 'bold' : 'normal' }}
              >
                {_.capitalize(tab)}
              </div>
            </EuiTab>
          ))}
        </EuiTabs>

        {/* Tab content */}
        <div style={{ paddingTop: theme.size.m }}>{currentTabContent}</div>

        {/* Project create modal */}
        {showModal === 'UPDATE_PROJECT' ? (
          <ProjectModal
            project={project}
            onClose={() => setShowModal(false)}
            fetch={_fetchProject}
          />
        ) : null}
      </Fragment>
    );
  })();
};

export default ProjectDetailsPage;
