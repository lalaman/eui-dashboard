import { useMemo } from 'react';

const InventoryTab = (props) => {
  const inventory = useMemo(() => props.inventory || [], [props.inventory]);

  return (
    <div>This is the inventory</div>
  );
};

export default InventoryTab;
