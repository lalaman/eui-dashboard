import { useRef, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {
  EuiButton,
  EuiCard,
  EuiForm,
  EuiFormRow,
  EuiFieldText,
  EuiPageTemplate,
  EuiFieldPassword,
  useEuiTheme
} from '@elastic/eui';
import { login } from '../store/user';

const LoginPage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { euiTheme } = useEuiTheme();
  const emailInput = useRef();
  const [username, setUsername] = useState({
    value: '',
    error: false
  });
  const [password, setPassword] = useState({
    value: '',
    error: false
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (emailInput) emailInput.current.focus();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setUsername({ value: username.value, error: !username.value });
    setPassword({ value: password.value, error: !password.value });

    if (![username, password].every((input) => !input.error && !!input.value))
      return;

    setLoading(true);
    dispatch(login(username.value, password.value))
      .then(() => {
        const queryString = window.location.search;
        const query = new URLSearchParams(queryString);
        const url = query.get('to')
          ? query
              .get('to')
              .replace(
                `${window.location.protocol}//${window.location.host}`,
                ''
              )
          : '/';
        navigate(url, { replace: true });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <EuiPageTemplate
      minHeight="100vh"
      paddingSize="none"
      pageContentProps={{
        verticalPosition: 'center',
        horizontalPosition: 'center',
        color: 'subdued'
      }}
    >
      <EuiCard paddingSize="xl" className="eui-textLeft" title="Welcome">
        <EuiForm id="login-form" component="form" onSubmit={handleSubmit}>
          {/* Username */}
          <EuiFormRow
            label="Username"
            helpText={
              username.error ? (
                <div style={{ color: euiTheme.colors.danger }}>
                  Enter a valid email
                </div>
              ) : null
            }
          >
            <EuiFieldText
              inputRef={emailInput}
              placeholder="Email"
              icon="user"
              name="username"
              type="email"
              value={username.value}
              isInvalid={username.error}
              onChange={(e) =>
                setUsername({
                  value: e.target.value,
                  error: false
                })
              }
            />
          </EuiFormRow>

          {/* Password */}
          <EuiFormRow
            label="Password"
            helpText={
              password.error ? (
                <div style={{ color: euiTheme.colors.danger }}>
                  Enter a password
                </div>
              ) : null
            }
          >
            <EuiFieldPassword
              placeholder="Password"
              type="dual"
              value={password.value}
              isInvalid={password.error}
              onChange={(e) =>
                setPassword({
                  error: false,
                  value: e.target.value
                })
              }
              aria-label="Password field"
            />
          </EuiFormRow>

          {/* Buttons */}
          <EuiButton color="primary" fill isLoading={loading} type="submit">
            Login
          </EuiButton>
        </EuiForm>
      </EuiCard>
    </EuiPageTemplate>
  );
};

export default LoginPage;
