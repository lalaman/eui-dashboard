import { useEffect, useMemo, useRef, useState } from 'react';
import _ from 'lodash';
import {
  EuiModal,
  EuiModalHeader,
  EuiModalHeaderTitle,
  EuiModalBody,
  EuiForm,
  EuiFormRow,
  EuiFieldText,
  EuiModalFooter,
  EuiButton,
  EuiButtonEmpty,
  EuiComboBox,
  useEuiFontSize
} from '@elastic/eui';
import ProjectService from '../services/project.service';
import DeveloperService from '../services/developer.service';
import { addToast } from './ToastController';

const ProjectModal = (props) => {
  const nameInput = useRef();
  const form = useRef();
  const developerInput = useRef();
  const [loading, setLoading] = useState(false);
  const [project, setProject] = useState({
    id: null,
    name: '',
    address: {
      street: '',
      street_alt: '',
      city: '',
      state: '',
      country: '',
      zip: ''
    }
  });
  const [developers, setDevelopers] = useState([]);
  const [developer, setDeveloper] = useState([]);
  const [status, setStatus] = useState(false);
  const closeModal = props.onClose;
  const fetchProjects = props.fetch;

  // Mounted
  useEffect(() => {
    fetchDevelopers();
  }, []);

  useEffect(() => {
    setProject({
      id: props?.project?.id || null,
      name: props?.project?.name || '',
      address: {
        street: props?.project?.address?.street || '',
        street_alt: props?.project?.address?.street_alt || '',
        city: props?.project?.address?.city || '',
        state: props?.project?.address?.state || '',
        country: props?.project?.address?.country || '',
        zip: props?.project?.address?.zip || ''
      },
      developer_id:
        props?.project?.developer_id ||
        props?.project?.developer?.id ||
        developer?.[0]?.value ||
        null
    });

    if (props?.project?.developer) {
      setDeveloper([
        {
          label: props.project.developer.name,
          value: props.project.developer.id
        }
      ]);
    }
  }, [props.project]);

  const validProject = useMemo(
    () => !!(project.developer_id && project.name && project.address.city),
    [project]
  );

  // Create project action
  const handleProjectAction = (ev) => {
    ev.preventDefault();
    setLoading(true);

    const createMode = !project.id;
    const query = createMode
      ? ProjectService.create(project)
      : ProjectService.update(project.id, project);

    query
      .then(res => {
        if (!res) throw new Error();
        fetchProjects();
        closeModal();
        addToast({
          title: `${createMode ? 'Created' : 'Updated'} project`,
          color: 'success',
          iconType: 'check',
          text: (
            <p>
              {createMode ? 'Created' : 'Updated'} project{' '}
              <strong>{project.name}</strong>
            </p>
          )
        });
      })
      .catch((err) => {
        console.log(err);
        addToast({
          title: `Failed to ${createMode ? 'create' : 'update'} project`,
          color: 'danger',
          iconType: 'cross',
          text: <p>Unable to {createMode ? 'create' : 'update'} project</p>
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const fetchDevelopers = () => {
    if (status === 'FETCH_DEVELOPERS') return;
    setStatus('FETCH_DEVELOPERS');
    DeveloperService.fetchAll()
      .then((res) => {
        setDevelopers(
          res.data.map((d) => ({
            label: d.name,
            value: d.id
          }))
        );
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setStatus(false);
      });
  };

  const handleCreateDeveloper = async (searchValue = '') => {
    if (
      document.activeElement !== developerInput?.current?.searchInputRefInstance
    ) {
      return;
    }
    const value = searchValue?.trim();
    if (!value) return;

    DeveloperService.create({ name: value })
      .then((res) => {
        const newDev = { label: res.data.name, value: res.data.id };
        const devs = [...developers, newDev];
        setDevelopers(devs);
        setDeveloper([newDev]);
        setProject({
          ...project,
          developer_id: newDev.value
        });
        addToast({
          title: 'Created developer',
          color: 'success',
          iconType: 'check',
          text: <p>Created new developer {value}</p>
        });
      })
      .catch((err) => {
        console.log(err);
        addToast({
          title: 'Failed to create developer',
          color: 'danger',
          iconType: 'cross',
          text: <p>Unable to create developer</p>
        });
      })
      .finally(() => {
        setStatus(false);
      });
  };

  const handleSelectDeveloper = (developer) => {
    setProject({ ...project, developer_id: developer?.[0]?.value });
    setDeveloper(developer);
  };

  return (
    <EuiModal onClose={props.onClose} initialFocus="[name=popswitch]">
      <EuiModalHeader>
        <EuiModalHeaderTitle
          style={{
            fontSize: useEuiFontSize('l').fontSize,
            fontWeight: 'bold'
          }}
        >
          {project?.id ? 'Update' : 'Create'} Project
        </EuiModalHeaderTitle>
      </EuiModalHeader>

      <EuiModalBody>
        <EuiForm
          id="project-modal"
          ref={form}
          component="form"
          onSubmit={handleProjectAction}
        >
          {/* Developer */}
          <EuiFormRow
            label="Developer"
            helpText="Select a developer or start typing to create a new one"
          >
            <EuiComboBox
              aria-label=""
              placeholder="Select/Create a developer"
              singleSelection={{ asPlainText: true }}
              options={developers}
              selectedOptions={developer}
              onChange={handleSelectDeveloper}
              isLoading={['CREATE_DEVELOPER', 'FETCH_DEVELOPERS'].includes(
                status
              )}
              ref={developerInput}
              onBlur={(e) => e.preventDefault()}
              isInvalid={!developer[0]}
              onCreateOption={handleCreateDeveloper}
              customOptionText="Create developer: {searchValue}"
            />
          </EuiFormRow>

          {/* Name */}
          <EuiFormRow label="Project name">
            <EuiFieldText
              inputRef={nameInput}
              value={project.name}
              isInvalid={!project.name}
              compressed
              onChange={(e) =>
                setProject((p) => ({ ...p, name: e.target.value }))
              }
            />
          </EuiFormRow>
          {
            /** Address fields */
            Object.keys(project.address)
              .filter((k) => k !== 'id')
              .map((field) => (
                <EuiFormRow
                  key={field}
                  label={(() => {
                    if (field === 'state') return 'State/Province';
                    if (field === 'zip') return 'Zip/Postal code';
                    return _.capitalize(field.replace('_', ' '));
                  })()}
                >
                  <EuiFieldText
                    value={project.address[field]}
                    compressed
                    isInvalid={field === 'city' && !project.address.city.trim()}
                    onChange={(e) =>
                      setProject((p) => ({
                        ...p,
                        address: { ...p.address, [field]: e.target.value }
                      }))
                    }
                  />
                </EuiFormRow>
              ))
          }
        </EuiForm>
      </EuiModalBody>

      <EuiModalFooter>
        <EuiButtonEmpty onClick={closeModal}>Cancel</EuiButtonEmpty>

        <EuiButton
          type="submit"
          form="project-modal"
          isLoading={loading}
          isDisabled={!validProject}
          fill
        >
          {(() => {
            if (loading) return '';
            return project.id ? 'Update' : 'Create';
          })()}
        </EuiButton>
      </EuiModalFooter>
    </EuiModal>
  );
};

export default ProjectModal;
