import { useEffect } from 'react';
import {
  EuiCard,
  EuiFlexGroup,
  EuiFlexItem,
  EuiLink,
  EuiAvatar,
  EuiPagination,
  useEuiTheme
} from '@elastic/eui';
import { useSelector, useDispatch } from 'react-redux';
import { setFetching, fetchList } from '../store/posts';

const Posts = () => {
  const posts = useSelector((state) => state.posts);
  const dispatch = useDispatch();

  const { euiTheme: theme } = useEuiTheme();

  useEffect(() => {
    // EQUIVALENT TO MOUNTED
    // console.log('activated');

    // EQUIVALENT TO componentWillUnmount
    return () => {};
  }, []);

  useEffect(() => {
    dispatch(fetchList());
  }, [dispatch]);

  return (
    <>
    <div
    style={{
      margin: '10vh auto 0',
      maxWidth: '500px',
      paddingBottom: theme.size.xl,
      opacity: posts.fetching ? 0.25 : 1
    }}
  >
    {posts?.list
      ? posts.list.map((p) => (
          <EuiCard
            key={p.id}
            className="eui-textLeft"
            title={p.title}
            hasBorder
            description={p.body}
            style={{ marginBottom: theme.size.l }}
          >
            <EuiFlexGroup justifyContent="spaceBetween" alignItems="center">
              <EuiFlexItem grow={false}>
                <div>
                  {p.tags.map((tag) => (
                    <EuiLink
                      key={tag}
                      style={{ marginRight: theme.size.s }}
                    >
                      #{tag}
                    </EuiLink>
                  ))}
                </div>
              </EuiFlexItem>
              <EuiFlexItem grow={false} className="eui-textRight">
                <EuiAvatar
                  size="m"
                  color={theme.colors.accentText}
                  type="space"
                  name={p.reactions.toString()}
                />
              </EuiFlexItem>
            </EuiFlexGroup>
          </EuiCard>
        ))
      : []}
  </div>

  {/* Pagination */}
  <EuiFlexGroup
    justifyContent="center"
    style={{ paddingBottom: theme.size.l, maxWidth: '100%' }}
  >
    <EuiPagination
      pageCount={posts.total / posts.items_per_page}
      activePage={posts.page}
      onPageClick={(activePage) => {
        window.scrollTo(0, 0);
        dispatch(setFetching(true));
        setTimeout(() => {
          dispatch(fetchList(activePage));
        }, 500);
      }}
    />
  </EuiFlexGroup>
  </>
  );
};

export default Posts;
