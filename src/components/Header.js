import {
  EuiHeader,
  EuiHeaderSectionItem,
  EuiHeaderLogo,
  EuiHeaderLink,
  EuiHeaderLinks,
  EuiToolTip,
  useEuiTheme,
  EuiFlexGroup
} from '@elastic/eui';
import { useDispatch } from 'react-redux';
import { logout } from '../store/user';

const AppHeader = () => {
  const { euiTheme: theme } = useEuiTheme();
  const dispatch = useDispatch();

  return (
    <EuiHeader position="fixed">
      <EuiFlexGroup
        justifyContent="spaceBetween"
        style={{ maxWidth: theme.breakpoint.l, margin: '0 auto' }}
      >
        <EuiHeaderSectionItem border="right">
          <EuiHeaderLogo iconType="logoUptime">IInside Condos</EuiHeaderLogo>
        </EuiHeaderSectionItem>

        <EuiHeaderSectionItem>
          <EuiHeaderLinks aria-label="Navigation links">
            <EuiToolTip position="bottom" content="Logout">
              <EuiHeaderLink
                iconType="push"
                style={{ color: theme.colors.text }}
                href="#"
                onClick={() => dispatch(logout())}
              />
            </EuiToolTip>
          </EuiHeaderLinks>
        </EuiHeaderSectionItem>
      </EuiFlexGroup>
    </EuiHeader>
  );
};

export default AppHeader;
