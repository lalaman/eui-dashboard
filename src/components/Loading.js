import {
  EuiPageTemplate,
  EuiLoadingLogo,
  useEuiBackgroundColorCSS
} from '@elastic/eui';

const Loading = () => {
  const colorStyles = useEuiBackgroundColorCSS();

  return (
    <EuiPageTemplate
      template="centeredBody"
      css={[colorStyles['subdued']]}
    >
      <EuiLoadingLogo logo="logoSecurity" size="xl" />
    </EuiPageTemplate>
  );
};

export default Loading;
