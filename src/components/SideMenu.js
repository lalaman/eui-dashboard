import {
  useEuiPaddingCSS,
  EuiFlexItem,
  EuiListGroup,
  EuiListGroupItem,
  useEuiTheme
} from '@elastic/eui';
import { NavLink } from 'react-router-dom';

const SideMenu = () => {
  const { euiTheme: theme } = useEuiTheme();

  const menuItems = [
    {
      label: 'Developers',
      href: '/developers',
      name: 'developers'
    },
    {
      label: 'Projects',
      href: '/projects',
      name: 'projects'
    },
    {
      label: 'Settings',
      href: '/settings',
      name: 'settings'
    }
  ];

  return (
    <EuiFlexItem
      grow={false}
      style={{ paddingRight: theme.size.xxl }}
      css={[useEuiPaddingCSS('vertical')['m']]}
    >
      <EuiListGroup gutterSize="none" color="text">
        {menuItems.map((item) => (
          <EuiListGroupItem
            key={item.name}
            size="s"
            color="text"
            label={
              <NavLink
                style={({ isActive }) => ({
                  ...isActive ? { fontWeight: 'bold' } : undefined,
                  color: theme.colors.text
                })}
                to={item.href}
                color="text"
              >
                {item.label}
              </NavLink>
            }
          />
        ))}
      </EuiListGroup>
    </EuiFlexItem>
  );
};

export default SideMenu;
