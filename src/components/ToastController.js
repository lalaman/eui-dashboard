import React, { useState } from 'react';
import { EuiGlobalToastList } from '@elastic/eui';

let addToastHandler;
let removeAllToastsHandler;

/**
 * Add toast
 * @param {string} toast.title
 * @param {string} toast.color - ['success', 'danger', 'warning', etc]
 * @param {string} iconType
 * @param {HTMLNode} text
 */
export function addToast(toast) {
  addToastHandler(toast);
}

export function removeAllToasts() {
  removeAllToastsHandler();
}

export const ToastController = () => {
  const [toasts, setToasts] = useState([]);

  addToastHandler = (toast) => {
    setToasts(toasts.concat({ ...toast, id: Date.now().toString() }));
  };

  const removeToast = (removedToast) => {
    setToasts(toasts.filter((toast) => toast.id !== removedToast.id));
  };

  removeAllToastsHandler = () => {
    setToasts([]);
  };

  return (
    <div style={{ maxWidth: 320 }}>
      <EuiGlobalToastList
        toasts={toasts}
        dismissToast={removeToast}
        toastLifeTimeMs={6000}
      />
    </div>
  );
};
