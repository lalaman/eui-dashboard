import _ from 'lodash';
import { appendIconComponentCache } from '@elastic/eui/es/components/icon/icon';

/* ADD ICON NAMES HERE */
const icons = [
  'alert',
  'arrow_left',
  'arrow_right',
  'boxes_horizontal',
  'check',
  'checkInCircleFilled',
  'cross',
  'eye',
  'eye_closed',
  'help',
  'lock',
  'logo_app_search',
  'logo_elastic',
  'logo_elastic_stack',
  'logo_uptime',
  'push',
  'user'
];

// One or more icons are passed in as an object of iconKey (string): IconComponent
appendIconComponentCache({
  ...icons.reduce((acc, _icon) => {
    const { icon } = require(`@elastic/eui/es/components/icon/assets/${_icon}`);
    return {
      ...acc,
      [_.camelCase(_icon)]: icon
    };
  }, {})
});
