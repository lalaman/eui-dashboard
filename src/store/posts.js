import { createSlice } from '@reduxjs/toolkit';

export const posts = createSlice({
  name: 'posts',
  initialState: {
    fetching: false,
    list: [],
    total: 0,
    page: 0,
    items_per_page: 10
  },
  // EQUIVALENT TO MAP MUTATIONS
  reducers: {
    _setPosts: (state, action) => {
      state.list = action.payload.posts;
      state.total = action.payload.total;
      state.page = parseFloat(action.payload.skip) / state.items_per_page;
      state.fetching = false;
    },
    _setFetching: (state, action) => {
      state.fetching = !!action.payload;
    }
  }
});

export const { _setPosts, _setFetching } = posts.actions;

// EQUIVALENT TO MAP ACTIONS
export const fetchList =
  (page = 0) =>
  async (dispatch) => {
    const url = `https://dummyjson.com/posts?limit=10&skip=${page * 10}`;
    const response = await window.fetch(url);
    const data = await response.json();
    dispatch(_setPosts(data));
  };

export const setFetching = (fetching) => (dispatch) => {
  dispatch(_setFetching(fetching));
};

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
// EQ OF MAP GETTERS
export const get = (state) => state.list;

export default posts.reducer;
