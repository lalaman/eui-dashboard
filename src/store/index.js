import { configureStore } from '@reduxjs/toolkit';
// import posts from './posts';
import user from './user';
import project from './project';

export const store = configureStore({
  reducer: {
    // posts,
    user,
    project
  }
});
