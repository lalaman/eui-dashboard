import { createSlice } from '@reduxjs/toolkit';
import ProjectService from '../services/project.service';

export const project = createSlice({
  name: 'project',
  initialState: {
    id: null
  },
  reducers: {
    setProject: (state, action) => {
      Object.keys(action.payload).forEach((k) => {
        state[k] = action.payload[k];
      });
    },
    unsetProject: (state) => {
      // state = { id: null };
    }
  }
});

export const { setProject, unsetProject } = project.actions;

// Actions
export const fetchProject = (projectId) => async (dispatch) => {
  const res = await ProjectService.fetch(projectId).catch(() => false);
  if (!res) return false;

  // Set project
  dispatch(setProject(res.data));

  return res.data;
};

export default project.reducer;
