import Cookies from 'universal-cookie';
import { createSlice } from '@reduxjs/toolkit';
import AuthService from '../services/auth.service';
const cookies = new Cookies();

export const user = createSlice({
  name: 'user',
  initialState: {
    contact: null
  },
  reducers: {
    _setUser: (state, action) => {
      state.contact = action.payload.contact;
    },
    _unsetUser: (state) => {
      state.contact = null;
    }
  }
});

export const { _setUser, _unsetUser } = user.actions;

export const login = (email, password) => async (dispatch) => {
  const res = await AuthService.login(email, password);

  // Set user
  const data = JSON.parse(atob(res.data.split('.')[1]));
  dispatch(_setUser(data));

  // Set cookie
  cookies.set('_token', res.data);
  return res.data;
};

export const logout = () => (dispatch) => {
  cookies.remove('_token');
  dispatch(_unsetUser());
  window.location.href = '/login';
}

export default user.reducer;
