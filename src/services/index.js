import axios from 'axios';
import config from '../config/config.json';
import { addToast } from '../components/ToastController';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const instance = axios.create({
  baseURL: config.server_url || 'http://localhost:4000'
});

instance.interceptors.request.use(request => {
  const token = cookies.get('_token');
  if (token) request.headers.authorization = token;
  return request;
});

instance.interceptors.response.use(
  (response) => response,
  (err) => {
    if (err.response.status === 401) {
      cookies.remove('_token');
      addToast({
        title: 'Token error',
        color: 'danger',
        text: 'Your token is either invalid or has expired'
      });
      const savedUrl = window.location.href;
      window.location.href = `/login?to=${savedUrl}`;
      return;
    }

    addToast({
      title: 'Error',
      color: 'danger',
      text:
        err.response?.data?.message || 'There was an error handling the request'
    });
  }
);

export default instance;
