import service from './index';

const DeveloperService = {
  fetchAll: () => service.get('/developer/all'),
  create: (payload) => service.post('/developer', payload),
  update: (developerId, payload) =>
    service.put(`/developer/${developerId}`, payload),
  remove: (developerId) => service.delete(`/developer/${developerId}`)
};

export default DeveloperService;
