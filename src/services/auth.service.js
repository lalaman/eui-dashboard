import service from './index';

const AuthService = {
  login: (email, password) =>
    service.post('/account/login', { email, password })
};

export default AuthService;
