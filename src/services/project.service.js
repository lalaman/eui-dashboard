import service from './index';

const ProjectService = {
  fetchAll: () => service.get('/project/all'),
  fetch: (projectId) => service.get(`/project/${projectId}`),
  create: (payload) => service.post('/project', payload),
  update: (projectId, payload) => service.put(`/project/${projectId}`, payload),
  uploadRenderings: (projectId, type, payload) =>
    service.post(`/project/${projectId}/renderings?type=${type}`, payload),
  removeRenderings: (projectId, payload) =>
    service.post(`/project/${projectId}/renderings/remove`, payload)
};

export default ProjectService;
