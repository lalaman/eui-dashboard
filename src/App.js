import React, { Suspense } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { EuiProvider } from '@elastic/eui';
import './App.css';
import '@elastic/eui/dist/eui_theme_light.css';
import { ToastController } from './components/ToastController';

import Loading from './components/Loading';
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';

const App = () => (
  <EuiProvider colorMode="light">
    <Router>
      <Suspense fallback={<Loading />}>
        <Routes>
          <Route exact path="/login" element={<LoginPage />} />
          <Route exact path="/*" element={<HomePage />} />
          {/*
            - Projects
            - Inventory
            - Inventory details page
          */}
        </Routes>
      </Suspense>
    </Router>
    <ToastController />
  </EuiProvider>
);

export default App;
